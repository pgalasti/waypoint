package com.galasti.waypoint.waypointservice.service;

import com.galasti.waypoint.waypointservice.api.Route;
import com.galasti.waypoint.waypointservice.api.RouteUpdate;
import com.galasti.waypoint.waypointservice.api.Waypoint;
import com.galasti.waypoint.waypointservice.error.RouteNotFoundException;
import com.galasti.waypoint.waypointservice.helper.WaypointUpdater;
import com.galasti.waypoint.waypointservice.repo.RouteRepo;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

@Component
@AllArgsConstructor
public class RouteService {

    private static final Logger log = LoggerFactory.getLogger(RouteService.class);

    private RouteRepo routeRepo;

    public RouteUpdate createRoute(final Waypoint waypoint) {

        List<Route> routes = routeRepo.findRoutesByLastUpdatedBefore(ZonedDateTime.now());
        final Route route = Route.builder().build();
        route.addWaypoint(waypoint);
        route.setLastUpdated(ZonedDateTime.now());
        routeRepo.save(route);

        log.info("Route {} created", route.getId());

        return new RouteUpdate(route.getId(), route.getLastUpdated());
    }

    public RouteUpdate addToRoute(final String routeUuid, final List<Waypoint> waypoints) {

        final Optional<Route> opRoute = this.routeRepo.findById(routeUuid);
        final Route route = opRoute.orElseThrow(RouteNotFoundException::new);

        route.addWaypoints(waypoints);
        route.setLastUpdated(ZonedDateTime.now());
        routeRepo.save(route);

        log.info("Added to Route {} with {} waypoints", route.getId(), waypoints.size());

        return new RouteUpdate(route.getId(), route.getLastUpdated());
    }

    public RouteUpdate updateRoute(final String routeUuid, final List<Waypoint> waypoints) {

        final Optional<Route> opRoute = this.routeRepo.findById(routeUuid);
        final Route route = opRoute.orElseThrow(RouteNotFoundException::new);

        WaypointUpdater.updateWaypoints(route, waypoints);
        route.setLastUpdated(ZonedDateTime.now());
        routeRepo.save(route);

        log.info("Updated Route {} with {} waypoints", route.getId(), waypoints.size());

        return new RouteUpdate(route.getId(), route.getLastUpdated());
    }

    public Route getRoute(final String routeUuid) {
        final Optional<Route> opRoute = this.routeRepo.findById(routeUuid);

        log.info("Fetching Route {}", routeUuid);
        return opRoute.orElseThrow(RouteNotFoundException::new);
    }
}
