package com.galasti.waypoint.waypointservice.repo;

import com.galasti.waypoint.waypointservice.api.Route;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;

@Repository
@Component
public interface RouteRepo extends MongoRepository<Route, String> {

    @Query("{lastUpdated: {$lt: ?0}}")
    List<Route> findRoutesByLastUpdatedBefore(ZonedDateTime dateTime);
}
