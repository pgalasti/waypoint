package com.galasti.waypoint.waypointservice.api;

import lombok.*;

import java.time.ZonedDateTime;

@ToString
@NoArgsConstructor
@AllArgsConstructor
public class RouteUpdate extends Meta {

    @Getter @Setter
    private String routeId;

    @Getter @Setter
    private ZonedDateTime lastUpdated;
}
