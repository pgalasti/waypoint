package com.galasti.waypoint.waypointservice.api;

import lombok.*;

import javax.persistence.Embeddable;
import javax.persistence.Entity;

@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class Position {

    @Getter @Setter
    private Double latitude;

    @Getter @Setter
    private Double longitude;

    @Getter @Setter
    private Double altitude;

    @Getter @Setter
    private Double radius;

    @Getter @Setter
    private Double heading;
}
