package com.galasti.waypoint.waypointservice.api;

import lombok.*;

import java.time.ZonedDateTime;

@ToString
@NoArgsConstructor
public class Meta {

    @Getter @Setter
    private String label;

    @Getter @Setter
    private ZonedDateTime dateTime;
}
