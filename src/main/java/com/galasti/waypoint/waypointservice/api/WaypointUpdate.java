package com.galasti.waypoint.waypointservice.api;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@ToString
@NoArgsConstructor
public class WaypointUpdate extends Meta {

    @Getter @Setter
    List<Waypoint> waypoints;
}
