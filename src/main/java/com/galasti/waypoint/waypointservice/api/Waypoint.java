package com.galasti.waypoint.waypointservice.api;

import lombok.*;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@Builder
public class Waypoint extends Meta {

    @Getter @Setter
    private String id;

    @Getter @Setter
    @Embedded
    private Position position;
}
