package com.galasti.waypoint.waypointservice.api;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.CollectionUtils;

import javax.persistence.ElementCollection;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@ToString
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "route")
public class Route {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid",
            strategy = "uuid")
    @Getter @Setter
    private String id;

    @Getter @Setter
    @ElementCollection
    private List<Waypoint> waypoints;

    @Getter @Setter
    private ZonedDateTime lastUpdated;

    public void addWaypoint(final Waypoint waypoint) {
        if(CollectionUtils.isEmpty(waypoints)) {
            this.waypoints = new ArrayList<>();
        }
        waypoint.setId(UUID.randomUUID().toString());
        this.waypoints.add(waypoint);
    }

    public void addWaypoints(final List<Waypoint> waypoints) {

        if(CollectionUtils.isEmpty(waypoints)) {
            this.waypoints = new ArrayList<>();
        }

        waypoints.forEach(waypoint -> waypoint.setId(UUID.randomUUID().toString()));
        this.waypoints.addAll(waypoints);
    }

}
