package com.galasti.waypoint.waypointservice.error;

public class WaypointNotFound extends RuntimeException {

    public WaypointNotFound() {
        super();
    }

    public WaypointNotFound(String message) {
        super(message);
    }
}
