package com.galasti.waypoint.waypointservice.helper;

import com.galasti.waypoint.waypointservice.api.Route;
import com.galasti.waypoint.waypointservice.api.Waypoint;
import com.galasti.waypoint.waypointservice.error.WaypointNotFound;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;

public class WaypointUpdater {

    protected WaypointUpdater() {
        throw new RuntimeException("WaypointUpdater should not have an instance.");
    }


    public static List<Waypoint> updateWaypoints(final Route route, List<Waypoint> waypoints) {

        final List<Waypoint> existingWaypoints = route.getWaypoints();
        if(CollectionUtils.isEmpty(existingWaypoints)) {
            throw new WaypointNotFound(String.format("Route {} contains no waypoints for updating!", route.getId()));
        }

        if(CollectionUtils.isEmpty(waypoints)) {
            return existingWaypoints;
        }

        waypoints.stream()
                .forEach(updatedWaypoint -> {
                    Optional<Waypoint> optionalWaypoint = existingWaypoints.stream()
                            .filter(existingWaypoint -> StringUtils.endsWithIgnoreCase(existingWaypoint.getId(), updatedWaypoint.getId()))
                            .findFirst();

                    Waypoint existingWaypoint = optionalWaypoint.orElseThrow(WaypointNotFound::new);
                    existingWaypoint.setPosition(updatedWaypoint.getPosition());
                    existingWaypoint.setDateTime(updatedWaypoint.getDateTime());
                    existingWaypoint.setLabel(updatedWaypoint.getLabel());
                });

        return route.getWaypoints();
    }
}
