package com.galasti.waypoint.waypointservice.endpoint;

import com.galasti.waypoint.waypointservice.api.Route;
import com.galasti.waypoint.waypointservice.api.RouteUpdate;
import com.galasti.waypoint.waypointservice.api.Waypoint;
import com.galasti.waypoint.waypointservice.api.WaypointUpdate;
import com.galasti.waypoint.waypointservice.service.RouteService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/waypoints/v1")
@AllArgsConstructor
public class WaypointController {

    private RouteService routeService;

    @GetMapping(path = "test")
    public String test() {
        return "Test";
    }

    @PostMapping(path = "route")
    public RouteUpdate createRoute(@RequestBody final Waypoint waypoint) {
        return routeService.createRoute(waypoint);
    }

    @PatchMapping(path = "route/{routeId}/add")
    public RouteUpdate add(@PathVariable(name = "routeId") final String routeUuid,
                       @RequestBody final WaypointUpdate waypointUpdate) {
        return routeService.addToRoute(routeUuid, waypointUpdate.getWaypoints());
    }

    @PatchMapping(path = "route/{routeId}/update")
    public RouteUpdate update(@PathVariable(name = "routeId") final String routeUuid,
                              @RequestBody final WaypointUpdate waypointUpdate) {
        return routeService.updateRoute(routeUuid, waypointUpdate.getWaypoints());
    }

    @GetMapping(path = "route/{routeId}")
    public Route getRoute(@PathVariable(name = "routeId") final String routeUuid) {
        return routeService.getRoute(routeUuid);
    }

}
