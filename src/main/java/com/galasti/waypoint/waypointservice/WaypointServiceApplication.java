package com.galasti.waypoint.waypointservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WaypointServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(WaypointServiceApplication.class, args);
    }

}
