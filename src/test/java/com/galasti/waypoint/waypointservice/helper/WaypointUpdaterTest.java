package com.galasti.waypoint.waypointservice.helper;

import com.galasti.waypoint.waypointservice.api.Position;
import com.galasti.waypoint.waypointservice.api.Route;
import com.galasti.waypoint.waypointservice.api.Waypoint;
import com.galasti.waypoint.waypointservice.error.WaypointNotFound;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class WaypointUpdaterTest {

    @Test
    void updateWaypoints_emptyRoute() {

        final Route emptyRoute = new Route();
        Assertions.assertThrows(WaypointNotFound.class, () -> {
            WaypointUpdater.updateWaypoints(emptyRoute, TEST_WAYPOINTS);
        });
    }

    @Test
    void updateWaypoints_emptyWaypoints() {
        final Route route = Route.builder().waypoints(TEST_WAYPOINTS).build();

        final List<Waypoint> originalList = new ArrayList<>(route.getWaypoints());
        final List<Waypoint> unchangedWaypoints = WaypointUpdater.updateWaypoints(route, new ArrayList<>());

        assertEquals(unchangedWaypoints, originalList);
    }

    private static List<Waypoint> TEST_WAYPOINTS = new ArrayList<>(){{
        add(Waypoint.builder()
                .id("Test 1")
                .position(new Position(1d, 1d, 1d, 1d, 1d))
                .build());

        add(Waypoint.builder()
                .id("Test 2")
                .position(new Position(2d, 2d, 2d, 2d, 2d))
                .build());

        add(Waypoint.builder()
                .id("Test 3")
                .position(new Position(3d, 3d, 3d, 3d, 3d))
                .build());
    }};

    private static List<Waypoint> generateUpdatedWaypoints(List<Waypoint> waypoints) {

        waypoints.stream()
                .forEach(waypoint -> {
                    waypoint.getPosition().setLatitude(waypoint.getPosition().getLatitude()+1);
                    waypoint.getPosition().setLongitude(waypoint.getPosition().getLongitude()+1);
                    waypoint.getPosition().setAltitude(waypoint.getPosition().getAltitude()+1);
                    waypoint.getPosition().setRadius(waypoint.getPosition().getRadius()+1);
                    waypoint.getPosition().setHeading(waypoint.getPosition().getHeading()+1);
                });

        return waypoints;
    }
}